#!/bin/sh

tag=$(date +"%Y.%m.%d.%H.%M")

git tag -a  ${tag} -m "$@"
