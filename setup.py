import os

from setuptools import setup, find_packages
import versioneer

here = os.path.abspath(os.path.dirname(__file__))

setup(
    name="pyrsf",
    author="aavbsouza",
    license="BSD",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="read and write madagascar rsf (regularized sampled format) files",
    python_requires=">=3.6",
    packages=find_packages(exclude=("tests",)),
    entry_points={
        "console_scripts": [
            "pyrsf_info=pyrsf.apps.pyrsf_info:main",
            "pyrsf_attr=pyrsf.apps.pyrsf_attr:main",
            "pyrsf_fill=pyrsf.apps.pyrsf_fill:main",
        ]
    },
)
