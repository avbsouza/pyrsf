import os
import pwd
import sys
import time
import zlib
from typing import Any, Optional, Union, IO

import numpy as np  # type: ignore

from .input import Input


class Output:
    """
    Input rsf files (reading)

    This class can open a rsf file and read its parameters (header) and its content(data)

    Remarks
    ---------
    Not every possible rsf can be created, for instance using ascii files for the
    data file is not possible. This class can only create traditional
    rsf files (header and binary data in different files).

    Attributes
    -----------
    dtype: type
        format of the data content
    fp_dat: file
        file object for the binary data file

    Methods
    --------
    put()
        Put an arbitrary value in the header file (key=value)
    putAxix()
        Put on axis n o d values
    close()
        Close all files associated with the object
    seek()
        Specify file offset
    filename()
        Return header or binary filename
    write()
        Write part or all of the contents of the binary data
    getChecksum()
        Return current "accumulated" checksum value
    dtype
        Return numpy.dtype
    """

    def __init__(
        self,
        file: str,
        data_path: str = None,
        dtype: type = np.float32,
        checksum: bool = True,
        overwrite: bool = False,
    ) -> None:

        if not overwrite:
            if os.path.isfile(file):
                raise RuntimeError(f"This file already exists, file:{file}")

        if os.path.sep in file:
            if not os.path.isdir(os.path.dirname(file)):
                raise RuntimeError(f"Could not find directory for: {file}")

        if data_path is not None:
            if not os.path.isdir(data_path):
                raise RuntimeError(f"Could not find directory {data_path}")

        if not file.endswith(".rsf"):
            file = file + ".rsf"

        if not overwrite:
            self.fp_hdr = open(file, "w")
            self.put(None, "open:" + self._build_header())
        else:
            self.fp_hdr = open(file, "a")

        self.checksum: Optional[int] = 0 if checksum else None

        self.fp_dat: IO[bytes] = self._open_binary_file(
            file, overwrite, dtype, data_path
        )
        self._dtype = self._dtype2dataType(dtype)
        if not overwrite:
            self.put("in", '"%s"' % (self.fp_dat.name))
            self.put("data_format", '"%s"' % (self._dtype[0]))
            self.put("esize", self._dtype[1])

    def _open_binary_file(
        self, file: str, overwrite: bool, dtype: Any, data_path: Optional[str]
    ):
        assert os.path.isfile(file), f"got file:{file}"
        open_mode: str = "wb"
        if overwrite:
            rsf = Input(file, mmap=False)
            bin_file = rsf.parameters["in"]
            assert os.path.isfile(bin_file), f"got bin_file:{bin_file}"
            assert dtype == rsf.dtype, f"got {dtype} != {rsf.dtype}"
            open_mode = "r+b"
        else:
            dpath_env = os.environ.get("DATAPATH")
            path: str = ""
            if dpath_env is None and data_path is None:
                path = os.getcwd()
            if dpath_env is not None:
                path = dpath_env
            if data_path is not None:
                path = data_path

            if not os.path.isdir(path):
                raise RuntimeError(f"Cannot find dir:{path}")

            bin_file = os.path.join(path, file + "@")
            open_mode = "wb"

        fp_dat: IO[bytes] = open(bin_file, open_mode)
        return fp_dat

    def put(self, key: Optional[str], val: Any) -> None:
        """
        Put a key value pair on the header file

        Parameters:
            key (Optional[str]): key name
            value (Any): value name

        """
        if key is None:
            print(f"{val}", file=self.fp_hdr)
            return
        print(f"{key}={val}", file=self.fp_hdr)

    def putAxis(self, axis: int, n: int, o: float, d: float) -> None:
        """
        Put an axis

        Parameters:
            axis (int): which axis to put
            n (int)   : axis size
            o (float) : axis origin
            d (float) : axis increment
        """
        self.put("n" + str(axis), n)
        self.put("o" + str(axis), o)
        self.put("d" + str(axis), d)

    def seek(self, offset: int = 0, whence: int = 0) -> None:
        """
        Seek file, offset bytes from begining (whence=0),
        current position (whence=1) and end of file (whence=2)

        Parameters:
            offset (int): Offset to change file pointer
            whence (int): Change point from start (0), current
                          position (1) or end of file (2)
        """
        if not self.fp_dat.closed:
            self.fp_dat.seek(offset, whence)

    def write(self, data: np.ndarray) -> None:
        """
        Write data to binary file at the current offset

        Parameters:
            data (np.ndarray) : numpy array to be written
        """
        assert data.dtype == self._getType(self._dtype[0])

        if not self.fp_dat.closed:
            data.tofile(self.fp_dat)
            if self.checksum is not None:
                self.checksum = zlib.crc32(data.data, self.checksum)

    @property
    def dtype(self) -> Any:
        """
        Return numpy.dtype for the output file
        """
        return self._getType(self._dtype[0])

    def filename(self, binary: bool = False, absolute: bool = False):
        """
        Return filenames associated with a rsf file

        Parameters:
            binary (bool): If true return the binary filename,
                           otherwise the header
            absolute (bool): If true return the absolute path

        Returns:
            (str) filename
        """
        if binary:
            fname = self.fp_dat.name
        else:
            fname = self.fp_hdr.name

        if absolute:
            fname = os.path.abspath(fname)

        return fname

    def close(self, ensure_checksum: bool = False, binary_only: bool = False) -> None:
        """
        close file

        Parameters:
            ensure_checksum (bool): recalculate checksum for all written data
            binary_only (bool)    : only close the binary file
        """
        fname = self.fp_dat.name
        if not self.fp_dat.closed:
            self.fp_dat.close()
        if binary_only:
            return
        self.put(None, "close: " + self._build_header())
        if ensure_checksum:
            self._checkChecksum(fname)
        else:
            if self.checksum is not None:
                self.put("checksum", "%X" % (self.checksum))
        self.fp_hdr.close()

    def getChecksum(self, hex: bool = False) -> Optional[Union[str, int]]:
        """
        Get current calculated checksumm

        Parameters:
            hex (bool): If true return checksum in hexdecimal forms

        Returns:
            (int) calculated checksum
        """
        if self.checksum is not None:
            return "%X" % (self.checksum) if hex else self.checksum
        else:
            return None

    def _checkChecksum(self, fname: str):
        assert os.path.isfile(fname)
        checksum = 0
        with open(fname, "rb") as fp:
            while True:
                buffer = fp.read(16384)
                checksum = zlib.crc32(buffer, checksum)
                if not buffer:
                    break
            if self.checksum is not None:
                self.put("checksum", "%X" % (self.checksum))
        if self.checksum is not None:
            if self.checksum != checksum:
                print(
                    "checksum error: self.checksum=%X != checksum=%X"
                    % (self.checksum, checksum)
                )

    def _dtype2dataType(self, dtype: type) -> tuple:
        if dtype == np.float32:
            return ("native_float", 4)
        elif dtype == np.float64:
            return ("native_double", 8)
        elif dtype == np.complex64:
            return ("native_complex", 8)
        elif dtype == np.int32:
            return ("native_int", 4)
        elif dtype == np.int64:
            return ("native_long", 8)
        elif dtype == np.byte:
            return ("native_byte", 1)
        else:
            raise RuntimeError("Unknownm dtype:{dtype}")

    def _getType(self, dataType: str) -> type:
        if dataType == "native_int":
            return np.int32
        elif dataType == "native_float":
            return np.float32
        elif dataType == "native_double":
            return np.double
        elif dataType == "native_complex":
            return np.complex64
        elif dataType == "native_long":
            return np.int64
        elif dataType == "native_byte":
            return np.byte
        else:
            raise RuntimeError(f"Unknown type found, got type:{type}")

    def _build_header(self) -> str:
        Prog = sys.argv[0]
        Cdir = os.getcwd()
        User = pwd.getpwuid(os.getuid()).pw_name
        Host = os.uname().nodename
        Time = time.ctime()
        ret = f"{Prog}\t{Cdir}:\t{User}@{Host}\t{Time}"
        return ret
