import argparse
import os
from typing import Union
from math import sqrt, fabs
import numpy as np
import pyrsf


def from_dict(d: dict[str, str], kind: str, type: type):
    "axis info form file_paramters dict"
    pars: dict[str, str] = {}
    for i in range(7):
        name: str = kind + str(i)
        if name in d:
            pars[name] = type(d[name])

    return pars


def size(shape: dict[str, int]) -> int:
    "product of shape dimensions"
    s: int = 1
    for _, v in shape.items():
        s = s * v
    return s


def getargs():
    "parse cli arguments"
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        description="Show header information for rsf file"
    )

    parser.add_argument(
        "-i",
        "--input",
        help="input rsf file",
        dest="input",
        default=None,
        type=str,
        required=True,
    )

    args = parser.parse_args()

    assert os.path.isfile(args.input), f"cannot find file:{args.input}"
    return args


def _calc_stats_unreduced(x: np.ndarray, p0: int) -> dict[str, Union[float, int]]:
    "calculate intermediate values to be displayed"
    t = x.astype(np.float64)
    val_sum = float(t.sum(axis=-1).sum(axis=-1).sum(axis=-1))
    val_sqr = float((t * t).sum(axis=-1).sum(axis=-1).sum(axis=-1))
    val_min = float(t.min())
    val_max = float(t.max())
    pos_min = int(t.argmin())
    pos_max = int(t.argmax())
    nonzero = (t != 0.0).sum()
    ret: dict[str, Union[float, int]] = {
        "sum": float(val_sum),
        "sqr": float(val_sqr),
        "min": float(val_min),
        "max": float(val_max),
        "argmin": int(pos_min),
        "argmax": int(pos_max),
        "p0": int(p0),
        "nonzero": int(nonzero),
        "numpoints": int(np.prod(t.shape)),
    }
    del t
    return ret


# pylint: disable=R0914
def _calc_stats_reduce(
    items: list[dict[str, Union[float, int]]], shape: list[int]
) -> dict[str, Union[float, int]]:
    "finish calculation of statistics"
    assert len(items) > 0, f"got {len(items)} <= 0"

    vsqr: float = 0.0
    vsum: float = 0.0
    vmin: float = items[0]["min"]
    vmax: float = items[0]["max"]
    N: int = 0
    p0min: int = 0
    p0max: int = 0
    Nz: int = 0
    offset: int = 1
    t = shape[1:]
    reversed(t)
    for i in t:
        offset = offset * i
    cmin: int = 0
    cmax: int = 0
    for item in items:
        vsum += item["sum"]
        vsqr += item["sqr"]
        vmin_prev: float = vmin
        vmax_prev: float = vmax
        vmin = float(min(vmin, item["min"]))
        vmax = float(max(vmax, item["max"]))
        N += int(item["numpoints"])
        Nz += int(item["nonzero"])

        if vmin_prev > vmin or cmin == 0:
            p0min = int(item["p0"] * offset + item["argmin"])
            cmin += 1

        if vmax_prev < vmax or cmax == 0:
            p0max = int(item["p0"] * offset + item["argmax"])
            cmax += 1

    rms = float(sqrt(vsqr / float(N)))
    mean = float(vsum / float(N))
    var = float(fabs(vsqr - float(N) * mean * mean) / float(N - 1))
    std = float(sqrt(var))
    norm = float(sqrt(vsqr))

    return {
        "mean": mean,
        "min": vmin,
        "max": vmax,
        "rms": rms,
        "var": var,
        "std": std,
        "norm": norm,
        "argmin": p0min,
        "argmax": p0max,
        "nonzero": Nz,
    }


def calculate(rsf, args):
    "run all caculations"
    data: list = []

    for i in range(rsf.data.shape[0]):
        x = rsf.data[i]
        t = _calc_stats_unreduced(x, i)
        data.append(t)

    print(f"rsf_file:{os.path.abspath(args.input)}")
    ret = _calc_stats_reduce(data, rsf.data.shape)

    print(f"in={rsf.file_parameters['in']}")
    for k, v in ret.items():
        if isinstance(v, float):
            print(f"{k}\t=\t{v:.6f}")
        elif k == "argmin":
            print(f"{k}\t=\t{v} [pos:{np.unravel_index(v, rsf.data.shape)}]")
        elif k == "argmax":
            print(f"{k}\t=\t{v} [pos:{np.unravel_index(v, rsf.data.shape)}]")
        else:
            print(f"{k}\t=\t{v}")
    print(f"Non zero samples {ret['nonzero']} of {np.prod(rsf.data.shape)} samples")


def main():
    "main function"
    args = getargs()
    rsf = pyrsf.Input(args.input)
    calculate(rsf, args)


if __name__ == "__main__":
    main()
