import argparse
import os
import pyrsf


def str2bool(v: str) -> bool:
    """
    string to boolen
    https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def from_dict(d: dict[str, str], kind: str, type: type):
    """
    extract axis info from file_parameters dict
    """
    pars: dict[str, str] = {}
    for i in range(7):
        name: str = kind + str(i)
        if name in d:
            pars[name] = type(d[name])

    return pars


def size(shape: dict[str, int]) -> int:
    """
    product of shape dims
    """
    s: int = 1
    for _, v in shape.items():
        s = s * v
    return s


def message(d: dict[str, str], args):
    """
    build message used to display the information
    """
    shape = from_dict(d, "n", int)
    origin = from_dict(d, "o", float)
    incr = from_dict(d, "d", float)
    label = from_dict(d, "label", str)
    print(f"input file:{os.path.abspath(args.input)}")

    print("shape:")
    for k, v in shape.items():
        print(f"{k}={v} ", end="")

    print("\n\naxis origin:")
    for k, v in origin.items():
        print(f"{k}={v} ", end="")

    print("\n\naxis sampling:")
    for k, v in incr.items():
        print(f"{k}={v} ", end="")

    print("\n\nlabel:")
    for k, v in label.items():
        print(f"{k}={v} ", end="")

    print('\n\nin={d["in"]}')

    if args.full:
        print("full args:")
        for k, v in d.items():
            print(f"{k}={v}")
    st_ret = os.path.getsize(d["in"])  # os.stat(args.input)
    expected_size = size(shape) * int(d["esize"])

    print(
        f"The input file has {st_ret} of {expected_size} bytes [{size(shape)}"
        f" elements of size {int(d['esize'])}]"
    )


def getargs():
    """
    parse command line arguments
    """
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        description="Show header information for rsf file"
    )

    parser.add_argument(
        "-i",
        "--input",
        help="input rsf file",
        dest="input",
        default=None,
        type=str,
        required=True,
    )
    parser.add_argument(
        "-f", "--full", help="input rsf file", dest="full", default=False, type=str2bool
    )

    args = parser.parse_args()

    assert os.path.isfile(args.input), f"cannot find file:{args.input}"
    assert args.full in [True, False], f"got {args.full}"
    return args


def main():
    """
    main function
    """
    args = getargs()
    rsf = pyrsf.Input(args.input, dict_only=True, mmap=False)
    message(rsf.file_parameters, args)


if __name__ == "__main__":
    main()
