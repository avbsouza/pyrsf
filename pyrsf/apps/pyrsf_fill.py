import os
from copy import deepcopy
import argparse
import pyrsf
import numpy as np


def getargs():
    "parse cli args"
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        description="create rsf files filled with values"
    )

    parser.add_argument(
        "-o",
        "--output",
        help="rsf output",
        dest="output",
        default="file.rsf",
        type=str,
        required=True,
    )

    parser.add_argument(
        "-k",
        "--kind",
        help="fill type (uniform ('random uniform'), gaussian ('rando gaussian'), constant",
        dest="kind",
        default="uniform",
        type=str,
        required=True,
    )

    parser.add_argument(
        "-s",
        "--shape",
        help="volume shape",
        dest="shape",
        default="100,200,300",
        type=lambda x: [int(i) for i in x.split(",")],
        required=True,
    )

    parser.add_argument(
        "--origin",
        help="axis origin",
        dest="origin",
        default="0.0,0.0,0.0",
        type=lambda x: [float(i) for i in x.split(",")],
        required=False,
    )
    parser.add_argument(
        "--sampling",
        help="axis sampling",
        dest="sampling",
        default="1.0,1.0,1.0",
        type=lambda x: [float(i) for i in x.split(",")],
        required=False,
    )

    parser.add_argument(
        "--mean",
        help="mean for gaussian noise",
        dest="mean",
        default=0.0,
        type=float,
        required=False,
    )

    parser.add_argument(
        "--std",
        help="std for gaussian noise",
        dest="std",
        default=1.0,
        type=float,
        required=False,
    )

    parser.add_argument(
        "--min",
        help="min for uniform noise",
        dest="min",
        default=-1.0,
        type=float,
        required=False,
    )

    parser.add_argument(
        "--max",
        help="max for uniform noise",
        dest="max",
        default=1.0,
        type=float,
        required=False,
    )

    parser.add_argument(
        "--constant",
        help="constant value",
        dest="constant",
        default=0.0,
        type=float,
        required=False,
    )

    parser.add_argument(
        "--seed",
        help="numpy random seed",
        dest="seed",
        default=42,
        type=int,
        required=False,
    )

    args = parser.parse_args()
    assert len(args.shape) == len(args.sampling) == len(args.origin)
    assert not os.path.isfile(args.output)
    print(args)
    return args


def main():
    "main function"
    args = getargs()
    c_shape = deepcopy(args.shape)
    o_shape = deepcopy(args.origin)
    s_shape = deepcopy(args.sampling)

    reversed(c_shape)
    reversed(o_shape)
    reversed(s_shape)

    np.random.seed(args.seed)
    rsf = pyrsf.Output(args.output)
    for i in range(c_shape[0]):
        if args.kind == "constant":
            x = np.zeros(c_shape[1:]) + args.constant
        elif args.kind == "uniform":
            x = np.random.uniform(args.min, args.max, c_shape[1:])
        elif args.kind == "gaussian":
            x = np.random.normal(args.mean, args.std, c_shape[1:])
        x = x.astype(np.float32)
        rsf.write(x)
    n = len(c_shape)
    for i, (s, o, d) in enumerate(zip(c_shape, o_shape, s_shape)):
        rsf.putAxis(n - i, s, o, d)
    rsf.close()


if __name__ == "__main__":
    main()
