import os
from time import time
import json
import threading as thr
from datetime import datetime as date
from typing import Optional, TextIO
import numpy as np  # type: ignore


class DistOutput:
    """
    Helper class for writing from many writers to one file
    Remarks
    --------
    This class assumes that the file to be written already exists.
    The write operations are performed using the syscall pwrite.
    All writes to the file must be disjunct or a race condition will ensue.

    Attributes
    -----------
    fname: str
        File name
    debug: bool
        If generate debug information
    idString: str
        Identitication string ([hostname]_[pid]_[tid])
    debugJson: bool
        If debug logs are written as json records
    fd_dat: int
        File descriptor
    fp_debug: file
        Debug file descriptor

    Methods
    --------
    writeAt()
        Write at a pre defined offset
    close()
        Close binary file and if enabled debugging file

    """

    def __init__(
        self,
        fname: str,
        debug: bool = False,
        debugPath: str = None,
        debugJson: bool = False,
    ) -> None:

        self.fname = fname
        self.debug = debug
        self.idString = f"{os.uname().nodename}_{thr.get_native_id()}_{os.getpid()}"
        self.hostname = f"{os.uname().nodename}"
        self.debugJson = debugJson

        if not fname.endswith(".rsf@"):
            raise RuntimeError(
                f"{self.fname} is not a binary rsf@ file on hostname:{self.idString}"
            )

        if not os.path.isfile(self.fname):
            raise RuntimeError(
                f"Hostname {self.hostname} cannot find file:{self.idString}"
            )

        self.fd_dat: int = os.open(fname, os.O_WRONLY)
        self.fp_debug: Optional[TextIO] = None  # type: ignore

        if self.debug:
            debugFile = f"{self.idString}.txt"
            if debugPath is not None:
                assert os.path.isdir(
                    debugPath
                ), f"got as debugPath={debugPath} on {self.idString}"
                debugFile = os.path.join(debugPath, debugFile)

            self.fp_debug = open(debugFile, "a")

            self._debugPrint(f"filename:{os.path.abspath(self.fname)}")
            self._debugPrint(f"idString:{self.idString}")

    def _debugPrint(self, msg) -> None:
        if self.debug:
            if self.debugJson:
                js = json.dumps(msg)
                print(js, file=self.fp_debug)
            else:
                print(msg, file=self.fp_debug)

    def _write(self, fd: int, dat: np.ndarray, offset: int) -> None:
        assert fd >= 0, f"got {fd}"
        assert offset >= 0, f"got {offset}"

        if dat.ndim >= 3:
            temp = dat.reshape((-1,) + dat.shape[-2:])
            offset_increment = temp[0].size * temp.dtype.itemsize
            for i3 in range(temp.shape[0]):
                ret = os.pwrite(fd, temp[i3], offset)
                assert (
                    ret == temp[i3].size * temp.dtype.itemsize
                ), f"got {ret} != {temp[i3].size*temp.dtype.itemsize}"
                offset += offset_increment
        else:
            ret = os.pwrite(fd, dat.tobytes(), offset)
            assert (
                ret == dat.size * dat.dtype.itemsize
            ), f"got {ret} != {dat.size*dat.dtype.itemsize}"

    def openFile(self, fname: str = None) -> int:
        """
        open file and return int fd descriptor

        Parameters:
            fname(str) : Optional,  file to be opened

        Return
            fd, int : file descriptor
        """
        if fname is not None:
            assert os.path.isfile(fname), f"got {fname}, at:{self.idString}"
            fd = os.open(fname, os.O_WRONLY)
        else:
            return -1
        return fd

    def writeAt(self, dat: np.ndarray, offset: int, fd: int = None) -> None:
        """
        Write a numpy.ndarray at a specified offset (from the start of file)

        Parameters:
            dat (np.ndarray): data to be written
            offset (int)    : start offset to begin writing dat
            fd (int)        : file descriptor, where to write
        """
        assert offset >= 0, f"got offset {offset} on {self.idString}"

        numBytes = dat.size * dat.itemsize
        actual_shape = dat.shape
        start = time()

        if fd is not None:
            assert fd > 0, f"got fd:{fd}, {self.idString}"
            self._write(fd, dat, offset)
        else:
            self._write(self.fd_dat, dat, offset)

        if self.debug:
            end = time()
            dt = end - start
            rate = numBytes / dt
            sizeMbs = numBytes / (1024 * 1024)
            rateMbs = rate / (1024 * 1024)

            msg = {
                "start": str(date.now()),
                "size": sizeMbs,
                "rate": rateMbs,
                "dt": dt,
                "shape": actual_shape,
                "offset": offset,
            }

            self._debugPrint(msg)

    def close(self) -> None:
        """
        close binary fule and if enabled debug file
        """
        os.close(self.fd_dat)

        if self.fp_debug is not None:
            if not self.fp_debug.closed:
                self._debugPrint(f"closing debug file at {date.now()}")
                self.fp_debug.close()
