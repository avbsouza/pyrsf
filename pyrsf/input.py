import os
import re
import struct
import zlib
from copy import deepcopy
from typing import Dict, List, Tuple, Optional, Any, Union

import numpy as np  # type: ignore


class Input:
    """
    Input rsf files (reading)

    This class can open a rsf file and read its parameters (header) and its content(data)

    Remarks
    ---------
    Not every possible rsf can be opened, for instance ascii files
    for the data file cannot be opened. Also when using the single
    file format (header and binary data in a single file) or
    when the input file is in xdr format it is not possible to use
    the mmap strategy due to alignment issues and or data conversion.

    Attributes
    -----------
    axis_size: list
        list containing axis size (number of elements)
    axis_orig: list
        list containing axis origin (first coordinate)
    axis_incr: list
        list containing axis increment ("distance" between points)
    file_dtype: type
        format of the [returned] data content
    fp_dat: file
        file object for the binary data file

    Methods
    --------
    data()
        Property. When possible contains a numpy memory mapping of the binary file
    dtype()
        Property. Return the format of the binary file
    get()
        Return a string for the specified key (for instance "in" or "n1")
    getInt()
        Return a int (cast of the string) for the specified key (for instance "n2" or "n1")
    getFloat()
        Return a float (cast of the string) for the specified key (for instance "d2" or "o1")
    seek()
        Specify file offset, does not impact the memory mapping
    headerOffset()
        Offset in bytes from start to the endof header
    filename()
        Return header or binary filename
    read()
        Read part or all of the contents of the binary data
    getChecksum()
        Return current "accumulated" checksum (if checksum is True)
    """

    def __init__(
        self,
        file: str,
        mmap: bool = True,
        old_parser: bool = False,
        checksum: bool = True,
        mmap_writeable: bool = False,
        dict_only: bool = False,
    ) -> None:
        assert os.path.isfile(file), f"Could not find/use: {file}"
        self.file_parameters: Dict[str, str] = {}
        text_content, self.fp_dat_offset = self._read_binary(file)
        self.input_file = file
        for line in text_content:
            if old_parser:
                self._process_line_old(line, self.file_parameters)
            else:
                self._process_line(line, self.file_parameters)

        if "in" not in self.file_parameters:
            raise RuntimeError(
                f"File: {file}, does not appear to be "
                f"a valid rsf file, missing key "
                f"'in=', [params:{self.file_parameters}]"
            )

        self.dict_only = dict_only

        if self.dict_only:
            return

        self.checksum: Optional[int] = 0 if checksum else None
        self.axis_size = self._get_axis("n", self.file_parameters, int)
        self.axis_orig = self._get_axis("o", self.file_parameters, float)
        self.axis_incr = self._get_axis("d", self.file_parameters, float)
        self._xdr_type = self._check_xdr(self.file_parameters)

        if self._xdr_type is not None:
            if self._xdr_type == "xdr_float":
                self._file_dtype = np.float32  # type: ignore
            elif self._xdr_type == "xdr_int":
                self._file_dtype = np.int32  # type: ignore
        else:
            self._file_dtype = self._getType(self.file_parameters)  # type: ignore

        if self.fp_dat_offset == 0 and self._xdr_type is None:
            assert os.path.isfile(
                self.file_parameters["in"]
            ), f"Cannot find file {self.file_parameters['in']}"
            if mmap_writeable:
                self.fp_dat = open(self.file_parameters["in"], "rb+")
            else:
                self.fp_dat = open(self.file_parameters["in"], "rb")
            rev_shape_list: List[int] = self.axis_size.copy()
            rev_shape_list.reverse()
            rev_shape: Tuple[int, ...] = tuple(rev_shape_list)
            rev_shape = tuple(filter(lambda x: x if x > 1 else None, rev_shape))
            self._data: Optional[Any] = None
            if mmap:
                if mmap_writeable:
                    self._data = np.memmap(
                        self.fp_dat,
                        dtype=self._file_dtype,
                        mode="r+",
                        order="C",
                        shape=rev_shape,
                    )
                else:
                    self._data = np.memmap(
                        self.fp_dat,
                        dtype=self._file_dtype,
                        mode="r",
                        order="C",
                        shape=rev_shape,
                    )
            else:
                self._data = None
            self.fp_dat.seek(0)
        else:
            self.fp_dat = open(file, "rb")
            self.fp_dat.seek(self.fp_dat_offset, 0)
            self._data = None

        self.axis_size = self._get_axis("n", self.file_parameters, int)
        self.axis_orig = self._get_axis("o", self.file_parameters, float)
        self.axis_incr = self._get_axis("d", self.file_parameters, float)

        self.axis_size, self.axis_orig, self.axis_incr = self._squeeze_axis(
            self.axis_size, self.axis_orig, self.axis_incr
        )

    @property
    def parameters(self) -> Dict[str, str]:
        """
        return dictionary of file parameters
        """
        return self.file_parameters

    @property
    def par_only(self) -> bool:
        """
        return True if only extract parameters otherwise False
        """
        return self.dict_only

    @property
    def data(self):
        """
        return possibly memory mapped view of the file
        """
        if self.par_only:
            return None
        if self._data is None:
            raise RuntimeError("memory mapping could not be created")
        else:
            return self._data

    @property
    def dtype(self) -> Optional[type]:
        """
        return dtype of file
        """
        if self.par_only:
            return None
        return self._file_dtype

    def get(self, param: str) -> str:
        """
        Return a value as string from the file_parameters dict
        """
        if param in self.file_parameters:
            return self.file_parameters[param]
        else:
            raise RuntimeError(f"Could not find param:{param} in file parameters dict")

    def getInt(self, param: str) -> int:
        """
        return a int value from the file_parameters dictionary
        """
        return int(self.get(param))

    def getFloat(self, param: str) -> float:
        """
        return a float value from the file_parameters dictionary
        """
        return float(self.get(param))

    def getAxis(self, axis: int):
        """
        return axis information as n:size, o:origin, d:increment
        """
        return (
            self.getInt("n" + str(axis)),
            self.getFloat("o" + str(axis)),
            self.getFloat("d" + str(axis)),
        )

    def seek(self, offset: int = 0, whence: int = 0) -> None:
        """
        Seek on data file

        Parameters:
            offset (int): Offset on File
            whence (int): Calculate offset from start(0), current position(1), end(2)

        Returns:
            None
        """
        if self.par_only:
            return
        self.fp_dat.seek(offset, whence)

    def read(self, shape: Tuple[int]) -> Optional[np.ndarray]:
        """
        Read from data file with shape

        Parameters:
            shape (Tuple[int]): Read a shape block from file

        Returns:
            Optional[np.ndarray]: Read data
        """
        if self.par_only:
            return None

        nelem: int = int(np.prod(np.asarray(shape)))

        if self._xdr_type is not None:
            nbytes: int = int(
                np.zeros(1, dtype=self._file_dtype).dtype.itemsize * nelem
            )
            data = self.fp_dat.read(nbytes)
            data_list: Tuple[Any, ...] = tuple()
            if self._xdr_type == "xdr_float":
                fmt = f">{len(data)//4}f"
                data_list = struct.unpack(fmt, data)
            elif self._xdr_type == "xdr_int":
                fmt = f">{len(data)//4}I"
                data_list = struct.unpack(fmt, data)
            tmp = np.asarray(data_list, dtype=self._file_dtype)

            if self.checksum is not None:
                self.checksum = zlib.crc32(tmp.data, self.checksum)

            return tmp.reshape(shape)
        else:
            assert nelem > 0, f"got nelem={nelem}"
            ret = np.fromfile(self.fp_dat, dtype=self._file_dtype, count=nelem).reshape(
                shape
            )

            if self.checksum is not None:
                self.checksum = zlib.crc32(ret.data, self.checksum)

            return ret

    def getChecksum(self, hex: bool = False) -> Optional[Union[str, int]]:
        """
        Get current calculated checksumm

        Parameters:
            hex (bool): If true return checksum in hexdecimal form

        Returns:
            (int) calculated checksum
        """
        ret: Optional[Union[str, int]] = None

        if self.par_only:
            return ret

        if self.checksum is not None:
            ret = "%X" % (self.checksum) if hex else self.checksum

        return ret

    def close(self) -> None:
        """
        close binary file, flushing the mmap mapping, if defined
        """
        if not self.par_only:
            if self._data is not None:
                self._data.flush()
            self.fp_dat.close()

    @property
    def headerOffset(self) -> Optional[int]:
        """
        return offset from begin of file to the start of the binary data
        """
        if self.par_only:
            return None
        return self.fp_dat_offset

    def filename(self, binary: bool = False, absolute: bool = False) -> str:
        """
        Return filenames associated with a rsf file

        Parameters:
            binary (bool): If true return the binary filename,
                           otherwise the header
            absolute (bool): If true return the absolute path

        Returns:
            (str) filename
        """
        if binary:
            fname = self.fp_dat.name
        else:
            fname = self.input_file

        if absolute:
            fname = os.path.abspath(fname)

        return fname

    def _process_line_old(self, line: str, params: dict):
        line_proc = (
            line.replace("\n", "")
            .replace('"', "")
            .replace('"', "")
            .replace("\t", "")
            .replace("#", "")
        )
        matches = re.findall(
            r"(\w*)[ \t]{0,}=[ \t]{0,}([a-zA-Z0-9_\/.@+-]*)", line_proc
        )
        self._fill_dict(params, matches)

    def _process_line(self, line: str, params: dict):
        if line.find("#") == 0:
            return
        indexes = [m.start() for m in re.finditer("=", line)]
        if len(indexes) == 0:
            return
        assert len(indexes) > 0, f"got len(indexes)={len(indexes)}"
        assert indexes[0] > 0, "got indexes[0]={indexes[0]}"

        idxs = [0] + indexes + [len(line)]
        for i in range(len(idxs) - 2):
            start = idxs[i]
            end = idxs[i + 2]
            section = line[start:end]
            matches_always = re.findall(
                r"(\w*)[ \t]{0,}=[ \t]{0,}([a-zA-Z0-9_\/.@+-]*)", section
            )
            if section.find('"') != -1:
                matches_cond = re.findall(
                    r"(\w*)[ \t]{0,}=[ \t]{0,}\"([\(\) a-zA-Z0-9_\/.@+-]*)\"", section
                )
            else:
                matches_cond = []
            matches = matches_always + matches_cond
            for k, v in matches:
                if k != "" and v != "":
                    params[k] = v

    def _fill_dict(self, params: dict, matches: list):
        if len(matches) == 0:
            return
        if len(matches) == 2:
            keys = matches[0]
            values = matches[1]
            assert len(keys) == len(values)
            for k, v in zip(keys, values):
                params[k] = v
        else:
            for k, v in matches:
                params[k] = v

    def _read_binary(self, filename: str):
        assert os.path.isfile(filename), f"got {filename}"
        text_content = []
        SF_EOL = b"\014"
        SF_EOT = b"\004"
        offset = 0
        with open(filename, "rb") as fp:
            for line in fp:
                if len(line) >= 3:
                    if (
                        line[0:1] == SF_EOL
                        and line[1:2] == SF_EOL
                        and line[2:3] == SF_EOT
                    ):
                        offset = 3 + fp.tell() - len(line)
                        break
                text_content.append(line.decode("utf-8"))
        return (text_content, offset)

    def _check_xdr(self, parameters: dict) -> Optional[str]:
        if "data_format" in parameters:
            data_type = parameters["data_format"]
            if data_type == "xdr_int":
                return "xdr_int"
            elif data_type == "xdr_float":
                return "xdr_float"
            else:
                return None
        else:
            raise RuntimeError("Could not key 'data_format' in file parameters")

    def _getType(self, parameters: dict) -> type:
        if "data_format" in parameters:
            data_type = parameters["data_format"]
            if data_type == "native_int":
                return np.int32
            elif data_type == "native_float":
                return np.float32
            elif data_type == "native_double":
                return np.double
            elif data_type == "native_complex":
                return np.complex64
            elif data_type == "native_long":
                return np.int64
            elif data_type == "native_byte":
                return np.byte
            else:
                raise RuntimeError(f"Unknown type found, got type:{type}")
        else:
            raise RuntimeError("Could not key 'data_format' in file parameters")

    def _get_axis(self, prefix: str, param: dict, cast: Optional[type] = None) -> list:
        ret: List[Any] = []
        assert cast is not None
        for i in range(9):
            key = prefix + str(i)
            if key in param:
                ret.append(cast(param[key]))
        return ret

    def _squeeze_axis(
        self, n: List[int], o: List[float], d: List[float]
    ) -> Tuple[List[int], List[float], List[float]]:
        assert len(n) >= len(o)
        assert len(n) >= len(d)
        nn, oo, dd = deepcopy(n), deepcopy(o), deepcopy(d)
        for _ in range(len(o), len(n)):
            oo.append(0.0)
        for _ in range(len(o), len(n)):
            dd.append(1.0)

        rn: List[int] = []
        ro: List[float] = []
        rd: List[float] = []

        for N, O, D in zip(nn, oo, dd):
            if N > 1:
                rn.append(N)
                ro.append(O)
                rd.append(D)
        return (rn, ro, rd)
