import pyrsf

import os
import numpy as np
from urllib.request import urlopen
import unittest
import tempfile
import hashlib
import zlib
import sys

try:
    import ray
except ImportError:
    pass


class marmousi_test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fp_tempfile = tempfile.NamedTemporaryFile(mode="wb")
        request = urlopen("http://ahay.org/data/marm/marmvel.hh")
        data_bytes = request.read()
        cls.fp_tempfile.write(data_bytes)
        cls.rsf = pyrsf.Input(cls.fp_tempfile.name)

    def test_axis1(self):
        n1, o1, d1 = (
            self.rsf.getInt("n1"),
            self.rsf.getFloat("o1"),
            self.rsf.getFloat("d1"),
        )
        self.assertEqual(n1, 751)
        self.assertEqual(o1, 0.0)
        self.assertEqual(d1, 4.0)

    def test_axis2(self):
        n2, o2, d2 = (
            self.rsf.getInt("n2"),
            self.rsf.getFloat("o2"),
            self.rsf.getFloat("d2"),
        )
        self.assertEqual(n2, 2301)
        self.assertEqual(o2, 0.0)
        self.assertEqual(d2, 4.0)

    def test_axis3(self):
        n3, o3, d3 = (
            self.rsf.getInt("n3"),
            self.rsf.getFloat("o3"),
            self.rsf.getFloat("d3"),
        )
        self.assertEqual(n3, 1)
        self.assertEqual(o3, 0.0)
        self.assertEqual(d3, 1.0)

    def test_extra(self):
        self.assertEqual(self.rsf.get("header"), "marmvel.header")
        self.assertEqual(self.rsf.get("title"), "Marmousi velocity (m/sec)")
        self.assertEqual(self.rsf.get("data_format"), "xdr_float")

    def test_content(self):
        true_id = "f4302792e84bb7ddbdc9d4d0f963b9df32d9f648960084884127e186119b99dd"
        self.rsf.seek(self.rsf.headerOffset)
        data = self.rsf.read((self.rsf.getInt("n2"), self.rsf.getInt("n1")))
        read_id = hashlib.sha256(data.tobytes()).hexdigest()
        self.assertEqual(true_id, read_id)

    def test_checksum_crc32(self):
        self.rsf.seek(self.rsf.headerOffset)
        data = self.rsf.read((self.rsf.getInt("n2"), self.rsf.getInt("n1")))
        self.assertEqual(self.rsf.getChecksum(), zlib.crc32(data.data, 0))
        self.assertEqual(self.rsf.getChecksum(), zlib.crc32(data.tobytes(), 0))
        self.assertEqual("%X" % (self.rsf.getChecksum()), "DBE182C9")

    def test_write_checksum_crc32(self):
        from uuid import uuid4 as randomName

        self.rsf.seek(self.rsf.headerOffset)
        data = self.rsf.read((self.rsf.getInt("n2"), self.rsf.getInt("n1")))
        oname = os.path.join("/tmp", str(randomName()) + ".rsf")
        rsfOut = pyrsf.Output(oname, data_path="/tmp")
        for i2 in range(self.rsf.getInt("n2")):
            rsfOut.write(data[i2, :])
        rsfOut.close(ensure_checksum=True)
        self.assertEqual("%X" % (rsfOut.getChecksum()), "DBE182C9")
        os.remove(oname)
        os.remove(oname + "@")


class galilee_test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fp_tempfile = tempfile.NamedTemporaryFile(mode="wb")
        request = urlopen("http://www.ahay.org/data/galilee/galilee.h")
        data_bytes = request.read()
        cls.fp_tempfile.write(data_bytes)
        cls.rsf = pyrsf.Input(cls.fp_tempfile.name, old_parser=True)

    def test_axis1(self):
        n1 = self.rsf.getInt("n1")
        self.assertEqual(n1, 3)

    def test_axis2(self):
        n2 = self.rsf.getInt("n2")
        self.assertEqual(n2, 132044)

    def test_axis3(self):
        n3 = self.rsf.getInt("n3")
        self.assertEqual(n3, 1)

    def test_extra(self):
        self.assertEqual(self.rsf.get("data_format"), "xdr_float")

    def test_content(self):
        true_id = "adf3908fbe501cd62a444fef2c9d863ef2b6f3426d19571cc0675638e8fa5590"
        self.rsf.seek(self.rsf.headerOffset)
        data = self.rsf.read((self.rsf.getInt("n2"), self.rsf.getInt("n1")))
        read_id = hashlib.sha256(data.tobytes()).hexdigest()
        self.assertEqual(true_id, read_id)

    def test_checksum_crc32(self):
        self.rsf.seek(self.rsf.headerOffset)
        data = self.rsf.read((self.rsf.getInt("n2"), self.rsf.getInt("n1")))
        self.assertEqual(self.rsf.getChecksum(), zlib.crc32(data.data, 0))
        self.assertEqual(self.rsf.getChecksum(), zlib.crc32(data.tobytes(), 0))
        self.assertEqual("%X" % (self.rsf.getChecksum()), "985A0EEA")

    def test_write_checksum_crc32(self):
        from uuid import uuid4 as randomName

        self.rsf.seek(self.rsf.headerOffset)
        data = self.rsf.read((self.rsf.getInt("n2"), self.rsf.getInt("n1")))
        oname = os.path.join("/tmp", str(randomName()) + ".rsf")
        rsfOut = pyrsf.Output(oname, data_path="/tmp")
        self.assertEqual(oname, rsfOut.filename(binary=False))
        self.assertEqual(oname + "@", rsfOut.filename(binary=True))
        for i2 in range(self.rsf.getInt("n2")):
            rsfOut.write(data[i2, :])
        rsfOut.close(ensure_checksum=True)
        self.assertEqual("%X" % (rsfOut.getChecksum()), "985A0EEA")
        os.remove(oname)
        os.remove(oname + "@")


class schlum_test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fp_tempfile = tempfile.NamedTemporaryFile(mode="wb")
        request = urlopen("http://ahay.org/data/vsp/schlum.HH")
        data_bytes = request.read()
        cls.fp_tempfile.write(data_bytes)
        cls.rsf = pyrsf.Input(cls.fp_tempfile.name)

    def test_axis1(self):
        n1 = self.rsf.getInt("n1")
        o1 = self.rsf.getFloat("o1")
        d1 = self.rsf.getFloat("d1")
        self.assertEqual(n1, 334)
        self.assertEqual(o1, 0.998)
        self.assertEqual(d1, 0.012)

    def test_axis2(self):
        n2 = self.rsf.getInt("n2")
        o2 = self.rsf.getFloat("o2")
        d2 = self.rsf.getFloat("d2")
        self.assertEqual(n2, 105)
        self.assertEqual(o2, 2425)
        self.assertEqual(d2, 25)

    def test_axis3(self):
        n3 = self.rsf.getInt("n3")
        o3 = self.rsf.getFloat("o3")
        d3 = self.rsf.getFloat("d3")
        self.assertEqual(n3, 1)
        self.assertEqual(o3, 2)
        self.assertEqual(d3, 1)

    def test_axis4(self):
        n4 = self.rsf.getInt("n4")
        o4 = self.rsf.getFloat("o4")
        d4 = self.rsf.getFloat("d4")
        self.assertEqual(n4, 1)
        self.assertEqual(o4, 0)
        self.assertEqual(d4, 1)

    def test_extra(self):
        self.assertEqual(self.rsf.get("data_format"), "xdr_float")
        self.assertEqual(self.rsf.getInt("esize"), 4)
        self.assertEqual(self.rsf.get("label1"), "time (s)")
        self.assertEqual(self.rsf.get("label2"), "depth (m)")

    def test_content(self):
        true_id = "48eede1f1460c2b721314939569387537d5c9e03110f019e5b31eb35d40053ff"
        data = self.rsf.read((self.rsf.getInt("n2"), self.rsf.getInt("n1")))
        read_id = hashlib.sha256(data.tobytes()).hexdigest()
        self.assertEqual(true_id, read_id)


class test_mmap_read_singleline_int(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fp_tempfile_hdr = tempfile.NamedTemporaryFile(mode="wb", buffering=0)
        cls.fp_tempfile_dat = tempfile.NamedTemporaryFile(mode="wb", buffering=0)
        cls.n1 = 11
        cls.o1 = -1.0
        cls.d1 = 0.1
        cls.n2 = 14
        cls.o2 = -3.0
        cls.d2 = 0.45
        cls.data = np.random.randint(-10, 10, size=(cls.n2, cls.n1), dtype=np.int32)
        cls.data.tofile(cls.fp_tempfile_dat)
        hdr = f"""n1={cls.n1} o1={cls.o1} d1={cls.d1} n2={cls.n2} o2={cls.o2} \
d2={cls.d2} esize=4 data_format="native_int" in="{cls.fp_tempfile_dat.name}"\n"""
        cls.fp_tempfile_hdr.seek(0)
        nbytes = cls.fp_tempfile_hdr.write(bytes(hdr.encode("utf-8")))
        cls.fp_tempfile_hdr.truncate(nbytes)
        cls.rsf = pyrsf.Input(cls.fp_tempfile_hdr.name)

    def test_axis1(self):
        n1 = self.rsf.getInt("n1")
        o1 = self.rsf.getFloat("o1")
        d1 = self.rsf.getFloat("d1")
        self.assertEqual(n1, self.n1)
        self.assertEqual(o1, self.o1)
        self.assertEqual(d1, self.d1)

    def test_axis2(self):
        n2 = self.rsf.getInt("n2")
        o2 = self.rsf.getFloat("o2")
        d2 = self.rsf.getFloat("d2")
        self.assertEqual(n2, self.n2)
        self.assertEqual(o2, self.o2)
        self.assertEqual(d2, self.d2)

    def test_content_read(self):
        n2, n1 = self.rsf.getInt("n2"), self.rsf.getInt("n1")
        data = self.rsf.read((n2, n1))
        self.assertEqual(data.dtype, np.int32)
        for i2 in range(n2):
            for i1 in range(n1):
                self.assertEqual(data[i2, i1], self.data[i2, i1])

    def test_content_mmap(self):
        n2, n1 = self.rsf.getInt("n2"), self.rsf.getInt("n1")
        for i2 in range(n2):
            for i1 in range(n1):
                self.assertEqual(self.rsf.data[i2, i1], self.data[i2, i1])


class test_mmap_read_singleline_float(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fp_tempfile_hdr = tempfile.NamedTemporaryFile(mode="wb", buffering=0)
        cls.fp_tempfile_dat = tempfile.NamedTemporaryFile(mode="wb", buffering=0)
        cls.n1 = 2
        cls.o1 = -1.0
        cls.d1 = 0.1
        cls.n2 = 5
        cls.o2 = -3.0
        cls.d2 = 0.45
        cls.data = np.random.normal(size=(cls.n2, cls.n1)).astype(np.float32)
        cls.data.tofile(cls.fp_tempfile_dat)
        hdr = f"""n1={cls.n1} o1={cls.o1} d1={cls.d1} n2={cls.n2} o2={cls.o2} d2={cls.d2} esize=4 data_format="native_float" in="{cls.fp_tempfile_dat.name}"\n"""
        cls.fp_tempfile_hdr.seek(0)
        nbytes = cls.fp_tempfile_hdr.write(bytes(hdr.encode("utf-8")))
        cls.fp_tempfile_hdr.truncate(nbytes)
        cls.rsf = pyrsf.Input(cls.fp_tempfile_hdr.name)

    def test_axis1(self):
        n1 = self.rsf.getInt("n1")
        o1 = self.rsf.getFloat("o1")
        d1 = self.rsf.getFloat("d1")
        self.assertEqual(n1, self.n1)
        self.assertEqual(o1, self.o1)
        self.assertEqual(d1, self.d1)

    def test_axis2(self):
        n2 = self.rsf.getInt("n2")
        o2 = self.rsf.getFloat("o2")
        d2 = self.rsf.getFloat("d2")
        self.assertEqual(n2, self.n2)
        self.assertEqual(o2, self.o2)
        self.assertEqual(d2, self.d2)

    def test_content_read(self):
        n2, n1 = self.rsf.getInt("n2"), self.rsf.getInt("n1")
        data = self.rsf.read((n2, n1))
        self.assertEqual(data.dtype, np.float32)
        for i2 in range(n2):
            for i1 in range(n1):
                self.assertEqual(data[i2, i1], self.data[i2, i1])

    def test_content_mmap(self):
        n2, n1 = self.rsf.getInt("n2"), self.rsf.getInt("n1")
        for i2 in range(n2):
            for i1 in range(n1):
                self.assertEqual(self.rsf.data[i2, i1], self.data[i2, i1])


class test_read_write(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fp_tempfile_rsf = tempfile.NamedTemporaryFile(
            mode="wb", buffering=0, suffix=".rsf", delete=False
        )
        cls.n1 = 200
        cls.o1 = -1.0
        cls.d1 = 0.1
        cls.n2 = 500
        cls.o2 = -3.0
        cls.d2 = 0.45
        cls.data = np.random.normal(size=(cls.n2, cls.n1)).astype(np.float32)
        cls.fp_tempfile_rsf.close()
        os.unlink(cls.fp_tempfile_rsf.name)
        out = pyrsf.Output(cls.fp_tempfile_rsf.name)
        out.write(cls.data)
        out.putAxis(1, cls.n1, cls.o1, cls.d1)
        out.putAxis(2, cls.n2, cls.o2, cls.d2)
        out.close()

    def test_read(self):
        rsf = pyrsf.Input(self.fp_tempfile_rsf.name)
        n1 = rsf.getInt("n1")
        n2 = rsf.getInt("n2")
        self.assertEqual(self.n1, n1)
        self.assertEqual(self.n2, n2)
        self.assertEqual(rsf.filename(binary=False), self.fp_tempfile_rsf.name)
        self.assertEqual(rsf.filename(binary=True), self.fp_tempfile_rsf.name + "@")
        data = rsf.read((n2, n1))
        for i2 in range(n2):
            for i1 in range(n1):
                self.assertEqual(rsf.data[i2, i1], data[i2, i1])
        for i2 in range(n2):
            for i1 in range(n1):
                self.assertEqual(rsf.data[i2, i1], self.data[i2, i1])
        rsf.close()

    @classmethod
    def tearDownClass(cls):
        rsf = pyrsf.Input(cls.fp_tempfile_rsf.name)
        inname = rsf.get("in")
        rsf.close()
        os.unlink(inname)
        os.unlink(cls.fp_tempfile_rsf.name)


class testDistOutput(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.x = np.random.normal(0, 1.0, (8, 100, 200, 300)).astype(np.float32)
        cls.tmpDir = tempfile.TemporaryDirectory()
        tmp = os.path.join(cls.tmpDir.name, "testDistOutput.rsf")
        cls.rsf = pyrsf.Output(tmp)

    def testDistOutput(self) -> None:
        n4, n3, n2, n1 = self.x.shape
        do = pyrsf.DistOutput(
            self.rsf.filename(binary=True, absolute=True),
            debug=True,
            debugPath=self.tmpDir.name,
        )
        for i4 in range(n4):
            do.writeAt(self.x[i4, :, :, :], i4 * n3 * n2 * n1 * self.x.itemsize)
        do.close()
        self.rsf.putAxis(1, n1, 0.0, 1.0)
        self.rsf.putAxis(2, n2, 0.0, 1.0)
        self.rsf.putAxis(3, n3, 0.0, 1.0)
        self.rsf.putAxis(4, n4, 0.0, 1.0)
        self.rsf.close()
        rsf = pyrsf.Input(self.rsf.filename(absolute=True))
        dat = rsf.read(self.x.shape)
        np.seterr(all="raise")
        self.assertTrue(np.allclose(dat, self.x))

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmpDir.cleanup()


class testDistOutputJson(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.x = np.random.normal(0, 1.0, (100, 200, 300)).astype(np.float32)
        cls.tmpDir = tempfile.TemporaryDirectory()
        tmp = os.path.join(cls.tmpDir.name, "testDistOutput.rsf")
        cls.rsf = pyrsf.Output(tmp)

    def testDistOutputJson(self) -> None:
        n3, n2, n1 = self.x.shape
        do = pyrsf.DistOutput(
            self.rsf.filename(binary=True, absolute=True),
            debug=True,
            debugPath=self.tmpDir.name,
            debugJson=True,
        )
        for i3 in range(n3):
            do.writeAt(self.x[i3, :, :], i3 * n2 * n1 * self.x.itemsize)
        do.close()
        self.rsf.putAxis(1, n1, 0.0, 1.0)
        self.rsf.putAxis(2, n2, 0.0, 1.0)
        self.rsf.putAxis(3, n3, 0.0, 1.0)
        self.rsf.close()
        rsf = pyrsf.Input(self.rsf.filename(absolute=True))
        dat = rsf.read(self.x.shape)
        np.seterr(all="raise")
        self.assertTrue(np.allclose(dat, self.x))

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmpDir.cleanup()


class testDistOutputJsonDebugPath(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.x = np.random.normal(0, 1.0, (100, 200, 300)).astype(np.float32)
        cls.tmpDir = tempfile.TemporaryDirectory()
        tmp = os.path.join(cls.tmpDir.name, "testDistOutput.rsf")
        cls.rsf = pyrsf.Output(tmp)

    def testDistOutputJsonDebugPath(self) -> None:
        n3, n2, n1 = self.x.shape
        do = pyrsf.DistOutput(
            self.rsf.filename(binary=True, absolute=True),
            debug=True,
            debugPath=None,
            debugJson=True,
        )
        for i3 in range(n3):
            do.writeAt(self.x[i3, :, :], i3 * n2 * n1 * self.x.itemsize)
        do.close()
        self.rsf.putAxis(1, n1, 0.0, 1.0)
        self.rsf.putAxis(2, n2, 0.0, 1.0)
        self.rsf.putAxis(3, n3, 0.0, 1.0)
        self.rsf.close()
        rsf = pyrsf.Input(self.rsf.filename(absolute=True))
        dat = rsf.read(self.x.shape)
        np.seterr(all="raise")
        self.assertTrue(np.allclose(dat, self.x))

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmpDir.cleanup()


@unittest.skipIf("ray" not in sys.modules, "requires ray library")
class testDistOutputRay(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.x = np.random.normal(0, 1.0, (100, 200, 300)).astype(np.float32)
        cls.tmpDir = tempfile.TemporaryDirectory()
        tmp = os.path.join(cls.tmpDir.name, "testDistOutput.rsf")
        cls.rsf = pyrsf.Output(tmp)

    def testDistOutputRay(self):
        ray.init(num_cpus=4)
        n3, n2, n1 = self.x.shape

        @ray.remote
        def parallelWriter(fname: str, tmpDir: str, idx: int, data: np.ndarray):
            do = pyrsf.DistOutput(fname, debugPath=tmpDir, debug=True)
            do.writeAt(data[idx, :, :], idx * n2 * n1 * data.itemsize)
            do.close()

        xput = ray.put(self.x)
        fname = self.rsf.filename(binary=True, absolute=True)
        futures = [
            parallelWriter.remote(fname, self.tmpDir.name, idx, xput)
            for idx in range(n3)
        ]
        ray.get(futures)
        ray.shutdown()
        self.rsf.putAxis(1, n1, 0.0, 1.0)
        self.rsf.putAxis(2, n2, 0.0, 1.0)
        self.rsf.putAxis(3, n3, 0.0, 1.0)
        self.rsf.close()

        rsf = pyrsf.Input(self.rsf.filename(absolute=True))
        dat = rsf.read(self.x.shape)
        np.seterr(all="raise")
        self.assertTrue(np.allclose(dat, self.x))

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmpDir.cleanup()


@unittest.skipIf("ray" not in sys.modules, "requires ray library")
class testDistOutputRayLong(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.x = np.random.normal(0, 1.0, (32, 100, 20, 30)).astype(np.float32)
        cls.tmpDir = tempfile.TemporaryDirectory()
        tmp = os.path.join(cls.tmpDir.name, "testDistOutput.rsf")
        cls.rsf = pyrsf.Output(tmp)

    def testDistOutputRay(self):
        ray.init(num_cpus=4)
        n4, n3, n2, n1 = self.x.shape

        @ray.remote
        def parallelWriter(fname: str, tmpDir: str, idx: int, data: np.ndarray):
            do = pyrsf.DistOutput(fname, debugPath=tmpDir, debug=True)
            do.writeAt(data[idx, :, :, :], idx * n3 * n2 * n1 * data.itemsize)
            do.close()

        xput = ray.put(self.x)
        fname = self.rsf.filename(binary=True, absolute=True)
        futures = [
            parallelWriter.remote(fname, self.tmpDir.name, idx, xput)
            for idx in range(n4)
        ]
        ray.get(futures)
        ray.shutdown()
        self.rsf.putAxis(1, n1, 0.0, 1.0)
        self.rsf.putAxis(2, n2, 0.0, 1.0)
        self.rsf.putAxis(3, n3, 0.0, 1.0)
        self.rsf.putAxis(4, n4, 0.0, 1.0)
        self.rsf.close()

        rsf = pyrsf.Input(self.rsf.filename(absolute=True))
        dat = rsf.read(self.x.shape)
        np.seterr(all="raise")
        self.assertTrue(np.allclose(dat, self.x))

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmpDir.cleanup()


class testParameterOnly(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.tmp_rsf = tempfile.NamedTemporaryFile(
            mode="wb", buffering=0, suffix=".rsf", delete=False
        )
        with open(cls.tmp_rsf.name, "w") as fp:
            print("in=/this/file/does/not/exist.rsf@", file=fp)
            print("n1=101", file=fp)
            print("n2=201", file=fp)

    def testParameterOnly(self):
        rsf = pyrsf.Input(self.tmp_rsf.name, dict_only=True)
        self.assertTrue(rsf.parameters["n1"] == str("101"))
        self.assertTrue(rsf.parameters["n2"] == str("201"))
        self.assertTrue(rsf.parameters["in"] == "/this/file/does/not/exist.rsf@")
        self.assertTrue(rsf.par_only == True)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmp_rsf.close()


class testOverWriteOutput(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.tmpDir = tempfile.TemporaryDirectory()
        cls.tmp_rsf = tempfile.NamedTemporaryFile(
            mode="wb",
            buffering=0,
            suffix=".rsf",
            delete=False,
        )
        cls.rsf_file = os.path.join(cls.tmpDir.name, cls.tmp_rsf.name)
        os.remove(cls.rsf_file)
        cls.shape = (10, 20, 30)
        rsf = pyrsf.Output(cls.rsf_file)
        x = np.random.uniform(-10.0, 10.0, cls.shape).astype(np.float32)
        cls.x_orig = x
        rsf.write(x)
        rsf.putAxis(1, cls.shape[2], 0.0, 1.0)
        rsf.putAxis(2, cls.shape[1], 0.0, 1.0)
        rsf.putAxis(3, cls.shape[0], 0.0, 1.0)
        rsf.close()
        print(cls.rsf_file)

    def testDontChange(self):
        from hashlib import sha256

        sha_original = sha256()
        sha_after = sha256()

        with open(self.rsf_file, "rb") as fp:
            sha_original.update(fp.read())

        rsf_out = pyrsf.Output(self.rsf_file, overwrite=True)

        with open(self.rsf_file, "rb") as fp:
            sha_after.update(fp.read())
        rsf_out.close()

        assert sha_original.hexdigest() == sha_after.hexdigest()

        rsf_in = pyrsf.Input(self.rsf_file)
        x = rsf_in.read(self.shape)
        assert np.allclose(x, self.x_orig)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmpDir.cleanup()


class testOverWriteOutput(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.tmpDir = tempfile.TemporaryDirectory()
        cls.tmp_rsf = tempfile.NamedTemporaryFile(
            mode="wb",
            buffering=0,
            suffix=".rsf",
            delete=False,
        )
        cls.rsf_file = os.path.join(cls.tmpDir.name, cls.tmp_rsf.name)
        os.remove(cls.rsf_file)
        cls.shape = (10, 20, 30)
        rsf = pyrsf.Output(cls.rsf_file)
        x = np.random.uniform(-10.0, 10.0, cls.shape).astype(np.float32)
        cls.x_orig = x
        rsf.write(x)
        rsf.putAxis(1, cls.shape[2], 0.0, 1.0)
        rsf.putAxis(2, cls.shape[1], 0.0, 1.0)
        rsf.putAxis(3, cls.shape[0], 0.0, 1.0)
        rsf.close()
        print(cls.rsf_file)

    def testOverwrite(self):
        from hashlib import sha256

        sha_original = sha256()
        sha_after = sha256()

        with open(self.rsf_file, "rb") as fp:
            sha_original.update(fp.read())

        rsf_out = pyrsf.Output(self.rsf_file, overwrite=True)

        with open(self.rsf_file, "rb") as fp:
            sha_after.update(fp.read())

        assert sha_original.hexdigest() == sha_after.hexdigest()

        x_new = np.random.uniform(-10.0, 10.0, self.shape).astype(np.float32)
        rsf_out.seek(0, 0)
        rsf_out.write(x_new)
        rsf_out.close()

        rsf_in = pyrsf.Input(self.rsf_file)
        x = rsf_in.read(self.shape)
        assert np.allclose(x, x_new), f"error for file:{self.rsf_file}"

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmpDir.cleanup()


class testOutputDtype(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        from uuid import uuid4 as randomName

        cls.dtypes = [np.float32, np.float64, np.int32, np.int64]
        cls.shape = (10, 20, 30)
        seedname = str(randomName())
        cls.fnames = [
            f"{seedname}_{kind}.rsf"
            for kind in ["float32", "float64", "int32", "int64"]
        ]
        assert len(cls.dtypes) == len(cls.dtypes)

    def testOutputDtype1(self):
        for fname, dtype in zip(self.fnames, self.dtypes):
            x = np.random.uniform(-100, 100, self.shape).astype(dtype)
            rsf = pyrsf.Output(fname, dtype=dtype)
            for i in range(len(self.shape)):
                rsf.putAxis(len(self.shape) - (i + 1), self.shape[i], 0.0, 1.0)
            rsf.write(x)
            assert rsf.dtype == dtype, f"got {rsf.dtype}"
            rsf.close()
            gen = pyrsf.Input(fname)
            r = gen.read(self.shape)
            assert gen.dtype == dtype, f"read dtype={gen.dtype}"
            gen.close()
            assert np.allclose(x, r)

    @classmethod
    def tearDownClass(cls) -> None:
        for fname in cls.fnames:
            os.remove(fname)
            os.remove(fname + "@")


if __name__ == "__main":
    unittest.main()
