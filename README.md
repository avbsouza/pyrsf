## pyrsf
[![pipeline status](https://gitlab.com/avbsouza/pyrsf/badges/master/pipeline.svg)](https://gitlab.com/avbsouza/pyrsf/commits/master)


pyrsf is a Python library that can be used to read and write rsf (Regularized Sampled Format) [Madagascar](https://github.com/ahay/src) files in pure Python, without needing to use the Madagascar C code.
Not all kinds of rsf files can be read or written by the library, for instance it not possible to read or write ascii datafiles.

## Install

The library only uses standard Python modules. The installation procedure should be painless:

``` bash
 pip install .
```

on the folder containing the `setup.py` file

## Usage

The following snippet  exemplifies the utilisation of the library

``` python
import pyrsf

rsfIn  = pyrsf.Input("input.rsf")
rsfOut = pyrsf.Output("output.rsf")

n1, o1, d1 = rsfIn.getAxis(1)
n2, o2, d2 = rsfIn.getAxis(2)
# use mmap interface
data = rsfIn.data[0:n2, 0:n1]*10.0

# use traditional read interface
data = rsf.read((n2,n1))
data = data[0:n2,0:n1]*10.0
rsfIn.close()

rsfOut.putAxis(1, n1, o1, d1)
rsfOut.putAxis(2, n2, o2, d2)
rsfOut.write(data)
rsfOut.close()
```

For normal Madagascar files (header and data in two separate files) is possible to use the [numpy.memmap](https://docs.scipy.org/doc/numpy/reference/generated/numpy.memmap.html) functionality to memory map rsf files to memory. This functionality is useful to read arbitrary slices of the data using the numpy syntax. Beware that sometimes is necessary or useful for perfomance reasons to convert the slice using [numpy.ascontiguousarray](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ascontiguousarray.html).

## Checksum

The default mode for the input and output classes from this module is to have the option `checksum=True`. This option calculate
the "accumulated" checksum (CRC32) for every read and write operation. This option is not expensive. Since the calculation
of the checksum is fast and there is no need for extra copies of the written or readed data. For the Output functions the
checksum is written on the header file as a Hex string on the field `checksum`. For both classes is possible to access the
current value of the checksum using the function `getChecksum`.

The following `C++` class can be used to generate checksums using the zlib library (the same library used by Python), including
the Hex representation (using the method `getString`).

```c++
#include <iostream>
#include <cstdio>
#include <climits>
#include <string>
#include <cstdio>
#include <stdexcept>
#include <vector>

#include <zlib.h>


class checkSumCRC32
{
public:
  checkSumCRC32 ():checksum_(0){}
  void put(const unsigned char*buf, const std::size_t elemSize, const std::size_t N)
  {
    if(N<=0){
      throw std::invalid_argument("N<=0");
    }
    if(buf == NULL){
      throw std::invalid_argument("buf == NULL");
    }
    const std::size_t Nbytes = N*elemSize;
    const std::size_t   umax = static_cast<std::size_t>(UINT_MAX);
    const std::size_t nchunk = Nbytes/umax;
    const std::size_t    rem = Nbytes%umax;
    for(std::size_t ichunk = 0; ichunk<nchunk; ++ichunk){
      checksum_ = crc32(checksum_, &buf[umax*ichunk], umax);
    }
    if(rem!=0){
      checksum_ = crc32(checksum_, &buf[umax*nchunk], rem);
    }
  }

  void reset(){checksum_ = 0;}

  unsigned int get(){return checksum_;}

  std::string getString(){
    char strCheckSum[256];
    std::snprintf(&strCheckSum[0], 256, "%X", checksum_);
    std::string str(strCheckSum);
    return str;
  };

private:
  unsigned int checksum_;
};

int main(void)
{
  const int n1=751, n2=2301;
  std::vector<float> data(n1*n2);
  std::fread(reinterpret_cast<void*>(data.data()), 751*2301, sizeof(float), stdin);
  checkSumCRC32 crc;
  crc.reset();
  for(int i2 = 0; i2<n2; ++i2){
    crc.put(reinterpret_cast<unsigned char*>(&data[i2*n1]), sizeof(float), n1);
  }

  std::cout<<"crc32[str]="<<crc.getString()<<" crc32[uint]="<<crc.get()<<std::endl;
  return 0;
}
```

These option are specially useful for small/medium files that are read in only one operation enabling
the verification of the file integrity before the use of its content.
